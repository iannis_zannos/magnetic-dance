
+ Object {
	>>> { | dancerName, index |
		// put object as reference into patternArray of dancer named dancerName
		var dancer;
		dancer = Dance.named(dancerName);
		if (dancer.notNil) {
			dancer.storeBeat(this, index);
		}{
			postf("there was no dancer named %\n", dancerName);
		}
		
	}
	
}
