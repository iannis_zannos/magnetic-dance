+ Dance {
	*makeKeyboardInputGui {
		var window;
		window = Window("type keys");
		// window.view.postln;
		
		View.globalKeyDownAction = { | view, char, mod, ascii |
			var degree;
			degree = ascii % 40 - 20;
			{ postf("sending from % this: %\n", localName, degree) } ! 10;
			this.broadcast(degree);
		};
		window.front;
		
	}
	
}