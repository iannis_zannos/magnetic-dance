/*
Load file startup.scd if it exists in same folder as this class definition.
This is useful because you do not have to copy a startup script to outside
the Extensions folder.
*/

LoadStartupScript {
	*initClass {
		StartUp add: {
			var scriptPath;
			scriptPath = PathName(this.filenameSymbol.asString).pathOnly ++ "startup.scd";
			if (File.exists(scriptPath)) {
				postf("Loading local script from:\n%\n", scriptPath);
				scriptPath.load;
				"Loaded script".postln;
			}{
				postf("startup file path not found\n%\n", scriptPath);			
			}
		}
	}
}
	
