/*
Main class for Magnetic Dance project
*/

Dance {
	classvar <partners;          // Dictionary of Dance instances, stored by name
	classvar <localName;         // Name of local partner
	classvar <dt = 1;            // Stream for getting times in polling task
	classvar <currentDt;
	classvar <task;              // Task that plays all partners in sync
	classvar <>verbose = true;   // post beat info at each beat
	var <name;
	var <>ip;
	var <beatArray;
	var <stream;
	var <oscFunc;
	var <thisBeat;


	*dt_ { | argDt |
		dt = argDt.asStream;
	}
	
	*initClass {
		partners = IdentityDictionary();
	}
	
	
	*addPartners { | name_ip_dict |
		name_ip_dict keysValuesDo: { | name, ip |
			Dance(name, ip);
		}
	}

	*startLocalProcess { | argLocalName |
		this.setLocalAddr(argLocalName);
		"STARTING LOCAL DANCE PROCESS".postln;
		Server.default.waitForBoot({
			task = Task({
				{
					partners do: _.playNextBeat;
					currentDt = dt.next;
					currentDt.wait;
				}.loop;
			}).play;
		})
	}

	*setLocalAddr { | argLocalName |
		// Use local addr to send to self.  Global addr may not work.
		localName = argLocalName.asSymbol;
		// Sending to Tokyo real addr did not work with the NAT scheme used in Tokyo
		// so using local address to send to self:
		partners[localName].ip = NetAddr.localAddr;
	}

	*startDataTest { | argLocalName |
		/* Test sending and receiving data with a forked OSC sending loop
			and tracing oscfunc.
		*/
		"================================================================".postln;
		postf("% starting data test\n", argLocalName);
		"================================================================".postln;
		this.setLocalAddr(argLocalName);
		OSCFunc.trace(true);
		{
			loop {
				this.broadcast(argLocalName, "is sending:", 1, 2, 3);
				0.25.wait;
			};
		}.fork;
	}

	*named { | name |
		^this.at(name)
	}
	*at { | name |
		^partners.at(name);
	}
	
	playNextBeat {
		thisBeat = stream.next;
		if (verbose) {
			postf("dancer: %, ip: %, beat: %\n", name, ip, thisBeat);
			postf("beats: %\n", beatArray);
		};
		thisBeat.value +> name;
	}

	storeBeat { | newValue, index |
		if (index.isNil) {
			thisBeat.value = newValue;
		}{
			beatArray.wrapAt(index).value = newValue;			
		}
	}

	*new { | name, ip |
		^this.newCopyArgs(name, NetAddr(ip, 57120)).init;
	}

	init {
		beatArray ?? { beatArray = { `nil } ! 12 };
		// enables running data tests:
		thisBeat = beatArray.first;
		stream = Pseq(beatArray, inf).asStream;
		partners[name] = this;
		oscFunc = OSCFunc({ | msg |
			postf("% received %\n", name, msg);
			// this is a first default for test purposes:
			thisBeat.value = (degree: msg[1], dur: currentDt);
		}, name);
	}

	////////////////////////////////////////////////////////////////
	// SENDING AND RECEIVING
	////////////////////////////////////////////////////////////////

	*broadcast { | ... data |
		partners do: _.send(data);
	}

	send { | data |
		// Send data to this partner, identifying the local dancer as partner
		ip.sendMsg(localName, *data);
	}
}
